const express = require('express');
const app = express();

app.use(express.json());

const users = [
    {id: 1, name: 'Izhar', lastname: 'Baeza', email: 'izhar.baeza@muruna.cl'}
];

app.get('/', (req, res) => {
    res.send('Api Node corriendo...');
});

app.get('/api/users', (req, res) => {
    res.send(users);
});

app.get('*', (req, res) => {
    res.status(404).send('Error 404 - Recurso no encontrado');
})

const port = process.env.SERVER_PORT || 80;
app.listen(port, () => console.log(`Listening on port ${port}`));